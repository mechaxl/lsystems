Introduction
============

This project aims to provide a simple interface for working with L-systems in the Rust programming language.

Examples
========

```rust
let mut system = LSystem::with_axiom("b");
system.add_rule('a', "ab");
system.add_rule('b', "a");

for step in 0..10 {
    println!("Step #{}: {}", step, system.next().unwrap());
}
```

```rust
let mut system = LSystem::with_axiom("baaaaaaa");
system.add_rule('b', "a");
system.add_context_rule((Some("b"), 'a', None), "b");

assert_eq!(system.next().unwrap(), "baaaaaaa");
assert_eq!(system.next().unwrap(), "abaaaaaa");
assert_eq!(system.next().unwrap(), "aabaaaaa");
assert_eq!(system.next().unwrap(), "aaabaaaa");
assert_eq!(system.next().unwrap(), "aaaabaaa");
assert_eq!(system.next().unwrap(), "aaaaabaa");
assert_eq!(system.next().unwrap(), "aaaaaaba");
assert_eq!(system.next().unwrap(), "aaaaaaab");
assert_eq!(system.next().unwrap(), "aaaaaaaa");
```
