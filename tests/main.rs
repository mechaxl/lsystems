extern crate lsystems;

use lsystems::LSystem;

/// Test that a basic example works
#[test]
fn basic_example() {
    let mut system = LSystem::with_axiom("b");
    system.add_rule('a', "ab");
    system.add_rule('b', "a");

    assert_eq!(system.next().unwrap(), "b");
    assert_eq!(system.next().unwrap(), "a");
    assert_eq!(system.next().unwrap(), "ab");
    assert_eq!(system.next().unwrap(), "aba");
    assert_eq!(system.next().unwrap(), "abaab");
    assert_eq!(system.next().unwrap(), "abaababa");
}

// Tests that a basic example of probability distributions works.
#[test]
fn test_distribution() {
    let mut system = LSystem::with_axiom("FG");
    system.add_rule('F', "F+");
    system.add_rule('F', "F-");
    system.add_rule('G', "G+");
    system.add_rule('G', "G-");

    system.add_distribution('F', Box::new(|vals: &Vec<String>| vals[0].clone()));
    system.add_distribution('G', Box::new(|vals: &Vec<String>| vals[1].clone()));

    assert_eq!(system.next().unwrap(), "FG");
    assert_eq!(system.next().unwrap(), "F+G-");
}

// Tests that closures and regular functions can be mixed and
// matched in the distributions hashmap.
#[test]
fn test_closures() {
    let mut system = LSystem::with_axiom("F");
    system.add_rule('F', "F+");

    fn distribution(vals: &Vec<String>) -> String {
        vals[0].clone()
    }

    system.add_distribution('F', Box::new(|vals: &Vec<String>| vals[0].clone()));
    system.add_distribution('G', Box::new(distribution));

    assert_eq!(system.next().unwrap(), "F");
    assert_eq!(system.next().unwrap(), "F+");
}

// Tests that `LSystem.reset` properly re-initializes the `LSystem`.
#[test]
fn test_reset() {
    let mut system = LSystem::with_axiom("F");
    system.add_rule('F', "FF");

    assert_eq!(system.next().unwrap(), "F");
    assert_eq!(system.next().unwrap(), "FF");
    assert_eq!(system.next().unwrap(), "FFFF");

    system.reset();

    assert_eq!(system.next().unwrap(), "F");
    assert_eq!(system.next().unwrap(), "FF");
    assert_eq!(system.next().unwrap(), "FFFF");
}

// Tests that a basic context-sensitive example works.
#[test]
fn test_context() {
    let mut system = LSystem::with_axiom("baaaaaaa");
    system.add_rule('b', "a");
    system.add_context_rule((Some("b"), 'a', None), "b");

    assert_eq!(system.next().unwrap(), "baaaaaaa");
    assert_eq!(system.next().unwrap(), "abaaaaaa");
    assert_eq!(system.next().unwrap(), "aabaaaaa");
    assert_eq!(system.next().unwrap(), "aaabaaaa");
    assert_eq!(system.next().unwrap(), "aaaabaaa");
    assert_eq!(system.next().unwrap(), "aaaaabaa");
    assert_eq!(system.next().unwrap(), "aaaaaaba");
    assert_eq!(system.next().unwrap(), "aaaaaaab");
    assert_eq!(system.next().unwrap(), "aaaaaaaa");
    assert_eq!(system.next().unwrap(), "aaaaaaaa");
}

// Tests that a context-sensitive rule takes precedence over
// a context-free rule.
#[test]
fn test_context_precedence() {
    let mut system = LSystem::with_axiom("abcb");
    system.add_rule('b', "x");
    system.add_context_rule((Some("c"), 'b', None), "y");

    assert_eq!(system.next().unwrap(), "abcb");
    assert_eq!(system.next().unwrap(), "axcy");
}

/// Branching and contexts don't work yet.
#[test]
#[should_panic]
fn test_branching_context() {
    let mut system = LSystem::with_axiom("ABC[DE][SG[HI[JK]L]MNO]");

    system.add_context_rule((Some("BC"), 'S', Some("G[H]M")), "X");

    assert_eq!(system.next().unwrap(), "ABC[DE][SG[HI[JK]L]MNO]");
    assert_eq!(system.next().unwrap(), "ABC[DE][XG[HI[JK]L]MNO]");
}
