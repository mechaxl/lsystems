extern crate lsystems;

use lsystems::LSystem;


fn main() {
    let mut system = LSystem::with_axiom("b");
    system.add_rule('a', "ab");
    system.add_rule('b', "a");

    for step in 0..10 {
        println!("Step #{}: {}", step, system.next().unwrap());
    }
}
